import React, { useEffect, useState } from "react";
import axios from "axios";

function App() {
  const [listOfUser, setListOfUser] = useState([]);
  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((r) => setListOfUser(r.data))
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  return (
    <div className="App">
      <h1>Liste des utilisateurs de l'api</h1>
      {listOfUser &&
        listOfUser.map((user, index) => (
          <div key={index} className="text-center my-5">
            <hr />
            <h3>nom : {user.name}</h3>
            <h3>prenom : {user.username}</h3>
            <h3>email : {user.email}</h3>
            <h3>nom de la compagnie : {user.company.name}</h3>
            <h3>telephone : {user.phone}</h3>
          </div>
        ))}
    </div>
  );
}

export default App;
